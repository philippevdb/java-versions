package java09;

public class Modularization {
    // module= collection of packages with a name. Modules explicitly mention what they need(require) and provide(export).

    // solves:
    // * more public than 'default', but less public than 'public'
    // * what dependencies do I have?
    // * large jars
    // * late runtime failures

    // in command line list all available modules : java --list-modules
    // module-info.java:
    // add per package requirements and exports
}
