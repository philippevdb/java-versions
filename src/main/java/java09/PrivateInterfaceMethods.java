package java09;

public interface PrivateInterfaceMethods {
    
    default int doSomething() { return doSomethingPrivately();}
    static int doSomethingStatically() {return doSomethingPrivatelyStatic();}

    private int doSomethingPrivately() {
        return 4;
    }

    private static int doSomethingPrivatelyStatic() {
        return 5;
    }
}
