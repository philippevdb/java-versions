package java09;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;

public class CompletableFuturesAddition {

    private static String test() {

        return "test";
    }

    public static void main(String[] args) {
        CompletableFuture<String> future = CompletableFuture.supplyAsync(CompletableFuturesAddition::test);
        future.thenAccept(System.out::println); // when found before timeout
        future.completeOnTimeout("timeout",1L, TimeUnit.SECONDS); // return on timeout of 1 second
        // alternatively already existed
        CompletableFuture.supplyAsync(CompletableFuturesAddition::test)
                .orTimeout(1L, TimeUnit.SECONDS) // keep in mind this throws Timeout exception
                .handle((message,exception) -> {
                    System.out.println(String.format("logging %s: %s", exception, message));
                    return "continue with this";
                })
                .thenAccept(System.out::println);
        // alternatively already existed
        CompletableFuture.supplyAsync(CompletableFuturesAddition::test)
                .orTimeout(1L, TimeUnit.SECONDS) // keep in mind this throws Timeout exception
                .whenComplete((string,exception) -> {
                    if (exception != null) {
                        System.out.println("log error " + exception);
                    } else {
                        System.out.println("my result " + string);
                    }
                });
        // alternatively already existed
        future.exceptionally(exception -> {
            System.out.println("handle exception alone" + exception);
            return "new result";
        });
    }
}
