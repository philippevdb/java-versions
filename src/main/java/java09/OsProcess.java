package java09;

public class OsProcess {

    public static void main(String[] args) {
        System.out.println("check processes");
        ProcessHandle current = ProcessHandle.current();
        System.out.println(current.info().startInstant());
        System.out.println(current.info().command());
        System.out.println(String.format("Process ID of current JVM %d", current.pid()));
    }
}
