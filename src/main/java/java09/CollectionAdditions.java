package java09;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class CollectionAdditions {

    public static void main(String[] args) {
        // creation of immutable collections contrary to Arrays.asList(..) which is mutable
        List.of(1,2,3);
        Set.of(1,2,3);
        Map.of("myKey1","myValue2","myKey2","myValue2");
    }
}
