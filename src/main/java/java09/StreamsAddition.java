package java09;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class StreamsAddition {

    public static void main(String[] args) {
        List<Integer> list = Arrays.asList(0,5,10,15,20,25);
        System.out.println("take while - process as long as predicate is still valid, then close the gate. Similar to limit().");
        list.stream().takeWhile(i ->i<20).forEach(System.out::println);
        System.out.println("drop while - as soon as predicate is valid, open the gate and process. Similar to skip().");
        list.stream().dropWhile(i ->i<20).forEach(System.out::println);
        System.out.println("range closed - last value is inclusive ");
        IntStream.rangeClosed(0,5).forEach(System.out::println);
        System.out.println("iterate - for loop in stream. Method is overloaded.");
        IntStream.iterate(0,i->i<=10,i->i+2).forEach(System.out::println);
        IntStream.iterate(0,i->i+2).limit(6).forEach(System.out::println); // keep in mind to add limit
        System.out.println("Optionals can stream()");
        List<Optional<String>> optionals = Arrays.asList(Optional.of("try"),Optional.empty(), Optional.of("test"));
        List<String> resultList = optionals.stream().flatMap(Optional::stream).collect(Collectors.toList());
        System.out.println("more awesome optional tricks");
        Optional.of("test").map(String::length).ifPresent(System.out::println);
        Optional<String> nonNullResult = optionals.stream()
                .flatMap(Optional::stream)
                .filter(string -> string == null)
                .findFirst()
                .or(()->Optional.of("non-null value"));
    }
}
