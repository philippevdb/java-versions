package java09;

public class REPLFunctionality {
    // this is addition to command line
    // start by typing 'jshell' into command line to enter jshell
    // you can leave with /exit or CTRL+D
    // all commands '/' talk to jshell, all commands without talk directly to the JVM
    // /help will give you options in jshell to explore
    // this is a way of writing commands in command line with java, as with other programming languages
    // can help with debugging, experimenting, prototyping, learning tool, ...

    // cool trick: type 'String test = "testing string"' ENTER
    // test. TAB -> gives options as in IDE
    // test.concat( -> TAB 2x gives javadoc
}
