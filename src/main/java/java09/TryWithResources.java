package java09;

public class TryWithResources {

    public static class Resources implements AutoCloseable {
        public Resources() {
            System.out.println("print something");
        }

        void doSomething() {
            System.out.println("do something");
        }

        @Override
        public void close() throws Exception {
            System.out.println("clean up");
        }
    }

    public static void main(String[] args) throws Exception {
        // can now be placed outside of resources
        // useful for dependency injection
        Resources resources = new Resources();
        try (resources){
            resources.doSomething();
        }
    }
}
