package java10;

import java.util.ArrayList;
import java.util.HashMap;

public class Variables {

    public static void main(String[] args) {
        // vars are only allowed locally in method, not as fields in class
        // vars can also not be used to express lambda expressions
        var x = 10; // type is inferred, can not be turned to string afterwards
        var map = new HashMap<String,Integer>();
        var list = new ArrayList<>();
        list.add(1);
        list.add("test"); // can add anything without specifying type
        map.put("",1);
    }
}
