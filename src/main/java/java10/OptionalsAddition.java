package java10;

import java.util.Optional;

public class OptionalsAddition {

    public static void main(String[] args) {
        var value = Optional.ofNullable(null).orElseThrow(() -> new RuntimeException("value was null"));
    }
}
