package java10;

import java.util.List;
import java.util.stream.Collectors;

public class CollectorsAddition {

    public static void main(String[] args) {
        // you can now collect to unmodifiable collections
        List.of(1,2,3,4).stream().collect(Collectors.toUnmodifiableList());
        var list = List.copyOf(List.of(1,2,3,4)); // you can also copy lists now
    }
}
