package java13;

public class SwitchExpression {

    public static void main(String[] args) {
        var number = switch(4) {
            case 1,2,3 -> 1000;
            case 4,5,6 -> {
                yield 1000 * 2;
            }
            default -> 3000;
        };
        System.out.println(number);
    }
}
