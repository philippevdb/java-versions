package java13;

public class TextBlock {

    public static void main(String[] args) {
        String html = """
                <html>
                    <body>
                        <p>this is a formatted text block.</p>
                        <p>No need to use \\n.</p>
                    </body>
                </html>
                """;
        String json = """
                {
                    "id": 1,
                    "name": "John Doe"
                }
                """;
        System.out.println(html);
        System.out.println(json);
    }
}
