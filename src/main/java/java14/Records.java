package java14;

public class Records {

    record Person(String firstName, String lastName) {};
    record Animal(String species) {
        static String noise = "woof";
    }

    public static void main(String[] args) {
        Person person = new Person("John","Doe");
        System.out.println("first name '" + person.firstName + "' is a final field");
        System.out.println("auto added: getter, equals, hashcode, tostring");
        System.out.println("Statics are also possible: " + Animal.noise);
    }
}
