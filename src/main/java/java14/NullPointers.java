package java14;

public class NullPointers {

    public static void main(String[] args) {
        try {
            String test = null;
            test.lines();
        } catch(NullPointerException e) {
            System.out.println("now tells you what exact object was null, not just line");
            System.out.println(e);
        }
    }
}
