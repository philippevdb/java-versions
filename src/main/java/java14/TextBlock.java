package java14;

public class TextBlock {

    public static void main(String[] args) {
        var text = """
                This text will appear \
                on same line.\sthis is due to the new escape sequences.\
                """;
        System.out.println(text);
    }
}
