package java11;

import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

public class StreamAdditions {

    public static void main(String[] args) {
        // not predicate
        List.of(1,2,3,4,5).stream().filter(Predicate.not(i -> i==3)).forEachOrdered(System.out::println);
    }
}
