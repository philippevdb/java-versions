package java11;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.OpenOption;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;

public class FilesAdditions {

    public static void main(String[] args) {
        try {
            Path path = Files.writeString(new File("/test.json").toPath(),"jsonString", StandardOpenOption.WRITE);
            String result = Files.readString(path);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
