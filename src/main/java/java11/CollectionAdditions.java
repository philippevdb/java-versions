package java11;

import java.util.List;

public class CollectionAdditions {

    public static void main(String[] args) {
        var list = List.of(1,2,3,4,5);
        Integer[] array = list.stream().toArray(Integer[]::new);
        Integer[] newArray = list.toArray(Integer[]::new);
    }
}
