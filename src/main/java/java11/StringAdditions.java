package java11;

import java.util.List;
import java.util.stream.Collectors;

public class StringAdditions {

    public static void main(String[] args) {
        System.out.println("isEmpty " + " ".isEmpty());
        System.out.println("isBlank " + " ".isBlank());
        String test = "one \n" + "two \n" + "three \n";
        List<String> lines = test.lines().collect(Collectors.toList());
        System.out.println("lines: " + lines);
        System.out.println("trimmed " + "      test ".strip()); // exaclty same as trim()
        System.out.println("trimmed " + "      test ".stripLeading());
        System.out.println("trimmed " + "      test         ".stripTrailing());
        System.out.println("trimmed " + "\t test \t".stripIndent());
        System.out.println("repeate " + "test ".repeat(3));
    }
}
