package java11;

import java.util.Optional;

public class OptionalAddition {

    public static void main(String[] args) {
        Optional<String> option = Optional.of("");
        boolean isEmpty = option.isEmpty(); // does opposite of isPresent, but for Strings alone, != isBlank
    }
}
