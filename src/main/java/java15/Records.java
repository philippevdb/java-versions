package java15;

public class Records {

    public record Machine(int inputs, int outputs) {
        // can't add fields
        // can override methods and create new methods
        public Machine {
            if (inputs > 5) {
                System.out.println("I'm logging from inside constructor");
            }
        }
    };

    public static void main(String[] args) {
        Machine machine = new Machine(10,5);

    }
}
