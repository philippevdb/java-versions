package java15;

public sealed class SealedClasses permits SubClass1 {
    // permit which classes can extend parent
    // children need to be in same module or package
}

final class SubClass1 extends SealedClasses {
    // subclasses of sealed classes need to be declared final, sealed or non-sealed
}

sealed interface SealedInterface {
    // this is possible as well
}

non-sealed class SubClass2 implements SealedInterface {

}
