package java12;

import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Locale;

public class CompactNumberFormat {

    public static void main(String[] args) {
        try {
            NumberFormat shortFormat = NumberFormat.getCompactNumberInstance(Locale.US,NumberFormat.Style.SHORT);
            System.out.println(shortFormat.format(1000L));
            System.out.println(shortFormat.parse("1M").doubleValue());
            NumberFormat longFormat = NumberFormat.getCompactNumberInstance(Locale.US,NumberFormat.Style.LONG);
            System.out.println(longFormat.format(1000));
            System.out.println(longFormat.parse("1 million"));
        } catch (ParseException e) {
        }
    }
}
