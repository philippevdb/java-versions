package java12;

public class StringIndent {
    public static void main(String[] args) {
        String test = "test";
        System.out.println(test);
        System.out.println(test.indent(10));
        System.out.println((String) test.transform(value -> new StringBuilder(value).reverse().toString()));
    }
}
