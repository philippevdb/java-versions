package java12;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class FileMismatch {

    static boolean filesAreIdentical(Path path1, Path path2) throws IOException {
        return Files.mismatch(path1,path2) == -1;
    }

    static long getFirstByteMismatch(Path path1, Path path2) throws IOException {
        return Files.mismatch(path1,path2);
    }
}
