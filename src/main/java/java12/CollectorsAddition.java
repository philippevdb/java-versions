package java12;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class CollectorsAddition {

    public static void main(String[] args) {
        double mean = Stream.of(1, 2, 3, 4, 5)
                .collect(Collectors.teeing(
                        Collectors.summingDouble(i -> i), // collector
                        Collectors.counting(),  // collector
                        (sum, count) -> sum / count)); // function combining collectors results
        System.out.println("mean: " + mean);
    }
}
