package java12;

public class Switches {

    public String getSomething(int value) {
        return switch (value) {
            case 0,1,2,3 -> "up to three";
            case 4,5,6,7 -> "between 4 and 7";
            default -> "number unknown";
        };
    }

    public void doSomething(int value) {
        switch (value) {
            case 0,1,2,3 -> System.out.println("up to three");
            case 4,5,6,7 -> System.out.println("between 4 and 7");
            case 8,9,10 -> {
                System.out.println("between 8 and 10");
            }
            default -> System.out.println("number unknown");
        }
    }
}
