package java12;

public class InstanceofChanges {

    public static void main(String[] args) {
        Object myString = "test";
        if (myString instanceof String newString) {
            System.out.println("combining old instanceof and cast: " + newString);
        }
    }
}
